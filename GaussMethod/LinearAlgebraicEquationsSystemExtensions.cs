﻿using GaussColumnMethod;
using LinearAlgebraicEquations;

namespace GaussMethod
{
    public static class LinearAlgebraicEquationsSystemExtensions
    {
        public static double[] Solve(this LinearAlgebraicEquationsSystem linearAlgebraicEquationsSystem)
        {
            var algebraicEquationsSystem = linearAlgebraicEquationsSystem.ForwardRecalculate();
            return algebraicEquationsSystem.Reverse();
        }

        private static LinearAlgebraicEquationsSystem ForwardRecalculate(
            this LinearAlgebraicEquationsSystem linearAlgebraicEquationsSystem)
        {
            var size = linearAlgebraicEquationsSystem.Size;
            var oldA = linearAlgebraicEquationsSystem.A;
            var oldB = linearAlgebraicEquationsSystem.B;
            var newA = oldA;
            var newB = oldB;

            for (var k = 0; k < size-1; k++)
            {
                for (var i = k+1; i < size; i++)
                {
                    for (var j = k+1; j < size; j++)
                    {
                        newA[i, j] = oldA[i, j] - (oldA[i, k] / oldA[k, k]) * oldA[k, j];
                    }

                    newB[i] = oldB[i] - (oldA[i, k] / oldA[k, k]) * oldB[k];
                }

                oldA = newA;
                oldB = newB;
            }
            
            return new LinearAlgebraicEquationsSystem(newA, newB);
        }
    }
}