﻿namespace Models
{

    public readonly struct Solution
    {
        public readonly double X1;
        public readonly double N1;
        public readonly double X2;
        public readonly double N2;
        public readonly double X3;
        public readonly double N3;

        public Solution(double x1, double n1, double x2, double n2, double x3, double n3)
        {
            X1 = x1;
            N1 = n1;
            X2 = x2;
            N2 = n2;
            X3 = x3;
            N3 = n3;
        }
    }
}