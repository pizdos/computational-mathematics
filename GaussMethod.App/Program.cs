﻿using System;
using LinearAlgebraicEquations;

namespace GaussMethod.App
{
    class Program
    {
        static void Main(string[] args)
        {
            var linearAlgebraicEquationsSystem =
                new LinearAlgebraicEquationsSystem(new double[,]
                {
                    {4, -3, 2},
                    {8, -8, 7},
                    {12, -5, 5}
                }, new double[]
                {
                    0, -12, 4
                });
            var solution = linearAlgebraicEquationsSystem.Solve();
        }
    }
}