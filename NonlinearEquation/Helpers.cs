﻿using System;

namespace NonlinearEquation
{
    public static class Helpers
    {
        public static readonly double PosInfinity = Math.Pow(10, 10);
        public const double NegInfinity = double.MinValue;
        
        public static (double, double) SolveQuadratic(double a, double b, double c)
        {
            var d = b * b - 4 * a * c;
            if (d < 0)
                throw new ArgumentException("Complex roots");
            var sqrt = Math.Sqrt(d);
            return ((-b - sqrt) / (2 * a), (-b + sqrt) / (2 * a));
        }
    }
}