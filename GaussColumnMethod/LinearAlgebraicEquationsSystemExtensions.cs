﻿using System;
using System.Linq;
using LinearAlgebraicEquations;

namespace GaussColumnMethod
{
    public static class LinearAlgebraicEquationsSystemExtensions
    {
        public static double[] Solve(this LinearAlgebraicEquationsSystem linearAlgebraicEquationsSystem)
        {
            
            Console.WriteLine(linearAlgebraicEquationsSystem.ToString());
            for (var i = 0; i < linearAlgebraicEquationsSystem.Size - 1; i++)
            {
                linearAlgebraicEquationsSystem.SortRows(i, i);
                linearAlgebraicEquationsSystem = linearAlgebraicEquationsSystem.Recalculate(i);
            }/*
            linearAlgebraicEquationsSystem.SortRows(0, 0); // todo i want more sorts
            Console.WriteLine("\n");
            Console.WriteLine(linearAlgebraicEquationsSystem.ToString());
            linearAlgebraicEquationsSystem = linearAlgebraicEquationsSystem.Recalculate(0);
            Console.WriteLine("\n");
            Console.WriteLine(linearAlgebraicEquationsSystem.ToString());
            linearAlgebraicEquationsSystem = linearAlgebraicEquationsSystem.Recalculate(1);
            Console.WriteLine("\n");
            Console.WriteLine(linearAlgebraicEquationsSystem.ToString());*/
            return linearAlgebraicEquationsSystem.Reverse();
        }
        
        public static LinearAlgebraicEquationsSystem Recalculate(
            this LinearAlgebraicEquationsSystem linearAlgebraicEquationsSystem, int zeroColumn)
        {
            var size = linearAlgebraicEquationsSystem.Size;
            var a = linearAlgebraicEquationsSystem.A;
            var b = linearAlgebraicEquationsSystem.B;

            var start = zeroColumn + 1;
            var firstVal = a[zeroColumn, zeroColumn];

            double[,] newA = a.Clone() as double[,] ?? throw new InvalidOperationException();
            double[] newB = b.Clone() as double[] ?? throw new InvalidOperationException();

            for (var i = start; i < size; i++)
            {
                for (var j = start; j < size; j++)
                {
                    newA[i, j] = a[i, j] - a[i, zeroColumn] / firstVal * a[zeroColumn, j];
                }

                newA[i, zeroColumn] = 0;
            }

            for (var i = start; i < size; i++)
            {
                newB[i] = b[i] - a[i, zeroColumn] / firstVal * b[zeroColumn];
            }

            return new LinearAlgebraicEquationsSystem(newA, newB);
        }


        /// <summary>
        /// only for usual triangle matrix
        ///  1 | 1 | 1 |
        ///  0 | 1 | 1 |
        ///  0 | 0 | 1 |
        /// </summary>
        /// <returns></returns>
        public static double[] Reverse(this LinearAlgebraicEquationsSystem linearAlgebraicEquationsSystem)
        {
            var size = linearAlgebraicEquationsSystem.Size;
            var a = linearAlgebraicEquationsSystem.A;
            var b = linearAlgebraicEquationsSystem.B;

            var results = Enumerable.Repeat(0.0, size).ToArray();
            for (var i = size - 1; i > -1; i--)
            {
                double sum = 0;
                for (var j = i; j < size; j++)
                {
                    sum += a[i, j] * results[j];
                }

                results[i] = (b[i] - sum) / a[i, i];
            }

            return results;
        }

        /// <summary>
        /// only for usual triangle matrix
        ///  1 | 0 | 0 |
        ///  1 | 1 | 0 |
        ///  1 | 1 | 1 |
        /// </summary>
        /// <returns></returns>
        public static double[] AntiReverse(this LinearAlgebraicEquationsSystem linearAlgebraicEquationsSystem)
        {
            var size = linearAlgebraicEquationsSystem.Size;
            var a = linearAlgebraicEquationsSystem.A;
            var b = linearAlgebraicEquationsSystem.B;

            var results = Enumerable.Repeat(0.0, size).ToArray();
            for (var i = 0; i < size; i++)
            {
                double sum = 0;
                for (var k = 0; k < i && i > 0; k++)
                {
                    sum += a[i, k] * results[k];
                }

                results[i] = (b[i] - sum) / a[i, i];
            }

            return results;
        }

        public static double[] GetNopy(this LinearAlgebraicEquationsSystem linearAlgebraicEquationsSystem,
            double[] results)
        {
            var size = linearAlgebraicEquationsSystem.Size;
            var a = linearAlgebraicEquationsSystem.A;
            var b = linearAlgebraicEquationsSystem.B;

            if (results.Length != size)
                throw new ArgumentException(nameof(results));

            var nopy = new double[size];

            for (var i = 0; i < size; i++)
            {
                double sum = 0;
                for (var j = 0; j < size; j++)
                {
                    sum += a[i, j] * results[j];
                }

                nopy[i] = sum - b[i];
            }

            return nopy;
        }
    }
}