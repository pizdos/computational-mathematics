﻿using System;

namespace SimpleIterationsNonlinearEquationMethod
{
    public class SimpleIterationsNonlinearEquation
    {
        public readonly Func<double, double> Fi;

        public SimpleIterationsNonlinearEquation(Func<double, double> fi)
        {
            Fi = fi;
        }
    }
}