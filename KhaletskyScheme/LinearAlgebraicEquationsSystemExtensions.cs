﻿using System;
using GaussColumnMethod;
using LinearAlgebraicEquations;
using Matrix;

namespace KhaletskyScheme
{
    public static class LinearAlgebraicEquationsSystemExtensions
    {
        public static double[] Solve(this LinearAlgebraicEquationsSystem linearAlgebraicEquationsSystem)
        {
            var (lMatrix, uMatrix) = linearAlgebraicEquationsSystem.Decomposition();
            var lMatrixSystem = new LinearAlgebraicEquationsSystem(
                lMatrix.Matrix2D, 
                linearAlgebraicEquationsSystem.B.Clone() as double[] ?? throw new InvalidOperationException()
            );
            var yReverse = lMatrixSystem.AntiReverse();
            
            var uMatrixSystem = new LinearAlgebraicEquationsSystem(
                uMatrix.Matrix2D, 
                yReverse
            );
            return uMatrixSystem.Reverse();
        }

        private static (LMatrix, UMatrix) Decomposition(this LinearAlgebraicEquationsSystem linearAlgebraicEquationsSystem)
        {
            var size = linearAlgebraicEquationsSystem.Size;
            if (size != 3)
                throw new NotSupportedException("Not enough info at ed materials");

            var lMatrix = new LMatrix(size);
            var uMatrix = new UMatrix(size);

            for (var i = 0; i < size; i++) uMatrix[0, i] = linearAlgebraicEquationsSystem.A[0, i];
            for (var i = 1; i < size; i++) lMatrix[i, 0] = linearAlgebraicEquationsSystem.A[i, 0] / uMatrix[0, 0];
            for (var i = 1; i < size; i++)
                uMatrix[1, i] = linearAlgebraicEquationsSystem.A[1, i] - lMatrix[1, 0] * uMatrix[0, i];
            lMatrix[2, 1] = (linearAlgebraicEquationsSystem.A[2, 1] - lMatrix[2, 0] * uMatrix[0, 1]) / uMatrix[1, 1];
            uMatrix[2, 2] = linearAlgebraicEquationsSystem.A[2, 2] -
                            lMatrix[2, 0] * uMatrix[0, 2] -
                            lMatrix[2, 1] * uMatrix[1, 2];
            return (lMatrix, uMatrix);
        }
    }
}