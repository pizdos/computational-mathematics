﻿namespace NonlinearEquation
{
    public readonly struct Factors4
    {
        public readonly double K1;
        public readonly double K2;
        public readonly double K3;
        public readonly double K4;

        public Factors4(double k1, double k2, double k3, double k4)
        {
            K1 = k1;
            K2 = k2;
            K3 = k3;
            K4 = k4;
        }
    }
}