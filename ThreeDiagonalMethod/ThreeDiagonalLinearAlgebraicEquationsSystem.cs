﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace ThreeDiagonalMethod
{
    public class ThreeDiagonalLinearAlgebraicEquationsSystem
    {
        public readonly int Size;
        [DisallowNull] public readonly double[] B;
        [DisallowNull] public readonly double[] C;
        [DisallowNull] public readonly double[] D;
        [DisallowNull] public readonly double[] R;

        public ThreeDiagonalLinearAlgebraicEquationsSystem([NotNull] double[] b, [NotNull] double[] c,
            [NotNull] double[] d, [NotNull] double[] r)
        {
            Size = b.Length;
            B = b;
            C = c;
            D = d;
            R = r;
            if (c.Length != Size || d.Length != Size || r.Length != Size)
            {
                throw new ArgumentException("Different length");
            }
        }
    }
}