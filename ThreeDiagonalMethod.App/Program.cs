﻿using System;

namespace ThreeDiagonalMethod.App
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var threeDiagonalLinearAlgebraicEquationsSystem = new ThreeDiagonalLinearAlgebraicEquationsSystem(new double[]
            {
                0,2,4,4,2
            }, new double[]
            {
                2,9,17,15,3
            }, new double[]
            {
                1,2,-4,-8,0
            }, new double[]
            {
                -10,-26,-16,-2,16
            });
            var solution = threeDiagonalLinearAlgebraicEquationsSystem.Solve();
        }
    }
}