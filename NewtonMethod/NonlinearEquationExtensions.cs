﻿using System;
using Models;
using NonlinearEquation;

namespace NewtonMethod
{
    public static class NonlinearEquationExtensions
    {
        public static Solution Solve(this NonlinearEquation.NonlinearEquation nonlinearEquation, int epsPow, double min, double max)
        {
            var accuracy = 1d / (10 * epsPow);

            var (root1, iterations1) = nonlinearEquation.Find(min, max, accuracy);
            return new Solution(root1, iterations1, 0,0,0,0);
        }

        public static double NewtonProcessElement(this NonlinearEquation.NonlinearEquation nonlinearEquation,
            double prevA)
        {
            var left = nonlinearEquation.Left;
            var firstDerivedFactors = nonlinearEquation.Factors.GetDerivedFactors4();
            var firstDerive = firstDerivedFactors.FuncWithFactors();
            return prevA - left(prevA) / firstDerive(prevA);
        }
        
        private static (double, int) Find(this NonlinearEquation.NonlinearEquation nonlinearEquation, double min, double max, double accuracy)
        {
            var left = nonlinearEquation.Left;
            var firstDerivedFactors = nonlinearEquation.Factors.GetDerivedFactors4();
            var secondDerivedFactors = firstDerivedFactors.GetDerivedFactors4();
            var secondDerive = secondDerivedFactors.FuncWithFactors();
            var iteration = 1;
            var c = 0.0;
            var err = Math.Abs(left(max) - left(min));
            if (secondDerive(min) * left(min) > 0)
                c = min;
            if (secondDerive(max) * left(max) > 0)
                c = max;
            for (; err > accuracy && left(c) != 0; iteration++)
            {
                var prevC = c;
                c = nonlinearEquation.NewtonProcessElement(c);
                err = Math.Abs(c - prevC);

                Console.WriteLine($"{iteration}//({min};{max}) ___ {c} ____ {err}\n");
            }

            return (c, iteration);
        }
    }
}