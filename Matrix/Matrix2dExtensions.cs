﻿using System;

namespace Matrix
{
    public static class Matrix2dExtensions
    {
        private const int DoubleSize = sizeof(double);

        public static double[,] Transpose(this double[,] arr)
        {
            var size = arr.GetSize();
            var newArr = new double[size, size];
            for (var i = 0; i < size; i++)
            {
                for (var j = 0; j < size; j++)
                {
                    newArr[i, j] = arr[j, i];
                }
            }

            return newArr;
        }
        
        public static void SwapRows(this double[,] arr, int line, int anotherLine)
        {
            var size = arr.GetSize();
            var tempArr = new double[size];
            Buffer.BlockCopy(arr, line * size * DoubleSize,
                tempArr, 0,
                size * DoubleSize);
            Buffer.BlockCopy(arr, anotherLine * size * DoubleSize,
                arr, line * size * DoubleSize,
                size * DoubleSize);
            Buffer.BlockCopy(tempArr, 0,
                arr, anotherLine * size * DoubleSize,
                size * DoubleSize);
        }

        public static void SwapElements(this double[] arr, int index, int anotherIndex)
        {
            var temp = arr[index];
            arr[index] = arr[anotherIndex];
            arr[anotherIndex] = temp;
        }

        public static int GetSize(this double[,] arr)
        {
            return arr.GetLength(0);
        }
        
        public static bool IsDiagonallyDominant(this double[,] arr)
        {
            var size = arr.GetSize();
            
            var haveStrict = false;

            for (var n = 0; n < size; n++)
            {
                var sum = 0.0;
                var nn = arr[n, n];

                for (var i = 0; i < size; i++)
                    if (i != n)
                        sum += Math.Abs(arr[n, i]);

                if (Math.Abs(sum) < nn)
                    return false;
                if (Math.Abs(sum) > nn)
                    haveStrict = true;
            }

            return haveStrict;
        }
    }
}