﻿using System;

namespace SimpleIterationsNonlinearEquationMethod.App
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var equation = new SimpleIterationsNonlinearEquation(x => 1 / Math.Sqrt(x + 1));
            var solution = equation.Solve(1000, -1, 2);
            Console.WriteLine($"{solution.X1} - {solution.N1}\n" +
                              $"{solution.X2} - {solution.N2}\n" +
                              $"{solution.X3} - {solution.N3}");
        }
    }
}