﻿using System;
using NonlinearEquation;

namespace CombinedMethod.App
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var equation = new NonlinearEquation.NonlinearEquation(new Factors4(1, 0, 7, 3));
            var solution = equation.Solve(1000, -1, 2);
            Console.WriteLine($"{solution.X1} - {solution.N1}\n" +
                              $"{solution.X2} - {solution.N2}\n" +
                              $"{solution.X3} - {solution.N3}");
        }
    }
}