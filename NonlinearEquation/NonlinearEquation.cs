﻿using System;

namespace NonlinearEquation
{
    public class NonlinearEquation
    {
        public readonly Func<double, double> Left;
        public readonly Factors4 Factors;

        public NonlinearEquation(Factors4 factors)
        {
            Left = factors.FuncWithFactors();
            Factors = factors;
        }
    }
}