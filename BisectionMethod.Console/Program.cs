﻿using NonlinearEquation;

namespace BisectionMethod.Console
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            System.Console.WriteLine("Hello World!");
            //var solver = new BisectionSolver(new Factors4(1, 0, 0, -1));
            var equation = new NonlinearEquation.NonlinearEquation(new Factors4(1, 0, -5, 2));
            var solution = equation.Solve(1000);
            System.Console.WriteLine($"{solution.X1} - {solution.N1}\n" +
                                     $"{solution.X2} - {solution.N2}\n" +
                                     $"{solution.X3} - {solution.N3}");
        }
    }
}