﻿using System;
using LinearAlgebraicEquations;

namespace GaussColumnMethod.App
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var linearAlgebraicEquationsSystem =
                new LinearAlgebraicEquationsSystem(new double[,]
                {
                    {4, -3, 2},
                    {8, -8, 7},
                    {12, -5, 5}
                }, new double[]
                {
                    0, -12, 4
                });
            var reverse = linearAlgebraicEquationsSystem.Solve();
            foreach (var d in reverse)
            {
                Console.Write($"{d} ");
            }
            Console.WriteLine();
            var nopy = linearAlgebraicEquationsSystem.GetNopy(reverse);
            foreach (var d in nopy)
            {
                Console.Write($"{d} ");
            }
        }
    }
}