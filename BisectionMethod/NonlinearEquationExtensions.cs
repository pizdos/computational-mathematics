﻿using System;
using Models;
using NonlinearEquation;

namespace BisectionMethod
{
    public static class NonlinearEquationExtensions
    {
        public static Solution Solve(this NonlinearEquation.NonlinearEquation nonlinearEquation, int epsPow)
        {
            var (min, max) = nonlinearEquation.Factors.LocalizeRoots();

            var accuracy = 1d / (10 * epsPow);

            if (Math.Abs(max - min) < 0.00000000001)
            {
                var (root, iterations) = nonlinearEquation.FindRoot(Helpers.NegInfinity, Helpers.PosInfinity, accuracy);
                return new Solution(root, iterations, 0, 0, 0, 0);
            } // check mb same roots

            var (root1, iterations1) = nonlinearEquation.FindRoot(Helpers.NegInfinity, min, accuracy);
            var (root2, iterations2) = nonlinearEquation.FindRoot(min, max, accuracy);
            var (root3, iterations3) = nonlinearEquation.FindRoot(max, Helpers.PosInfinity, accuracy);
            return new Solution(root1, iterations1, root2, iterations2, root3, iterations3);
        }

        private static (double, int) FindRoot(this NonlinearEquation.NonlinearEquation nonlinearEquation, double min, double max, double accuracy)
        {
            var distance = Math.Abs(max - min);
            var err = distance;
            var c = 0d;
            var iteration = 1;
            for (; err > accuracy && nonlinearEquation.Left(c) != 0; iteration++)
            {
                c = (min + max) / 2;
                (min, max) = nonlinearEquation.SelectInterval(min, max, c);
                err = max - min;
            }

            return (c, iteration);
        }

        private static (double, double) SelectInterval(this NonlinearEquation.NonlinearEquation nonlinearEquation, double a, double b, double c) =>
            nonlinearEquation.Left(a) * nonlinearEquation.Left(c) < 0 ? (a, c) : (c, b);
    }
}