﻿using System;

namespace Gauss_Method
{
    class Program
    {
        static double[,] MatrixA;           //Основная матрица СЛАУ
        static double[] MatrixB;            //Матрица свободных членов
        static double[] MatrixX;            //Матрица неизвестных переменных
        static int[] index;                 //Массив индексов 
        static uint size;                    //Размерность СЛАУ
        static bool SolveFound = true;      //Флаг начилия решения
        static double eps = 0.0001;         //Порядок точности для сравнения вещественных чисел

        static void header() //Заголовок. Сделано чисто для красоты, никакой смысловой нагрузки не несёт
        {
            Console.WriteLine("===Программа, предназначенная для решения Системы Линейный Алгебраических Уравнений (СЛАУ) Методом Гаусса===");
            Console.WriteLine();
        }

        static uint checkIn_Int() //uInt (беззнаковый Int) функция проверки введёных данных
        {
            uint check; //Целочисленная переменная
            bool flag; //Флаг для проверки
            do
            {
                flag = UInt32.TryParse(Console.ReadLine(), out check); //Пытаемся парсить полученную строку в переменную
                if (!flag)
                    Console.WriteLine("Введены некорректные данные! Попробуйте ещё раз!"); //Выводим сообщение об ошибке, если не получилось
            } while (!flag); //Цикл повторяется до тех пор, пока флаг не станет true
            return check;  //Возвращаем полученное значение
        }

        static double checkIn_Double() //Double функция проверки введёных данных
        {
            double check; //Целочисленная переменная
            bool flag; //Флаг для проверки
            do
            {
                flag = Double.TryParse(Console.ReadLine(), out check); //Пытаемся парсить полученную строку в переменную
                if (!flag)
                    Console.WriteLine("Введены некорректные данные! Попробуйте ещё раз!"); //Выводим сообщение об ошибке, если не получилось
            } while (!flag); //Цикл повторяется до тех пор, пока флаг не станет true
            return check;  //Возвращаем полученное значение
        }

        static void Main(string[] args)
        {
            header();

            Console.Write("Введите размерность уравнения: ");
            size = checkIn_Int();
            //Инициализируем массивы с размерностью, полученной с консоли.
            MatrixA = new double[size, size];
            MatrixB = new double[size];
            MatrixX = new double[size];
            index = new int[size];

            

            Console.WriteLine("Заполняем основную матрицу A:");
            for (var i = 0; i < size; i++)
            {
                for (var j = 0; j < size; j++)
                {
                    Console.Write($"A[{ i+1 }, { j+1 }]=");
                    MatrixA[i, j] = checkIn_Double();
                }
            }

            Console.WriteLine("Заполняем матрицу свободных членов B:");
            for (var i = 0; i < size; i++)
            {
                Console.Write($"B[{ i + 1 }]=");
                MatrixB[i] = checkIn_Double();
                index[i] = i; //Инициализирую массив индексов сразу здесь, чтобы не перегружать код циклами
            }

            Console.Clear(); //Очищаем консоль
            header();
           
            Console.WriteLine("Полученная матрица");
            for (var i = 0; i < size; i++)
            {
                for (var j = 0; j < size; j++)
                    Console.Write($"{MatrixA[i, j]} \t");
                Console.WriteLine($"| {MatrixB[i]}");
            }

            Console.WriteLine();
            Solve(); //Вызываем функцию решения
            
            if (SolveFound != false) //Вывод ответа, если решение найдено
            {
                Console.Write("Ответ: (");
                for (var i = 0; i < size; i++)
                {
                    if (i != size - 1) Console.Write($"{MatrixX[i]}; ");
                    else Console.Write($"{MatrixX[i]})");
                }
            }
        }

        static void Solve() //Решение
        {
            Forward(); //Вызываем функцию прямого хода метода
            Backward(); //Вызываем функцию обратного хода метода
        }

        static double FindRow(int row) //Поиск главного элемента матрицы
        {
            var maxIndex = row; //Максимальный индекс
            var max = MatrixA[row, index[maxIndex]]; //Макисмальное значение по этому индексу
            var ABSmax = Math.Abs(max); //Модуль максимального значения
            for (var currentIndex = row + 1; currentIndex < size; currentIndex++)
            {
                var current = MatrixA[row, index[currentIndex]]; //Выбранное значение
                var ABScurrent = Math.Abs(current); //Его модуль
                if (ABScurrent > ABSmax)    //Сравниваем выбранное с максимальным
                {                           //Если больше, то выбранное становится максимальным
                    maxIndex = currentIndex;
                    max = current;
                    ABSmax = ABScurrent;
                }
            }

            if (ABSmax < eps)
            {
                SolveFound = false;
                if (Math.Abs(MatrixB[row]) > eps)
                    Console.WriteLine("Система уравнений несовместна.");
                else
                    Console.WriteLine("Система уравнений имеет множество решений.");
            }

            //Меняем местами индексы
            var temp = index[row];
            index[row] = index[maxIndex];
            index[maxIndex] = temp;

            return max; //Возвращаем полученное значение
        }

        static void Forward()   //Прямой ход метода для приведения матрицы в треугольный вид
        {                       //Движение происходит по каждой строке сверху вниз
            for (var i = 0; i < size; i++)
            {
                //Выбираем главный элемент матрицы
                var r = FindRow(i); //В функцию передаётся текущее i, которое используется в ней для доступа к элементам массивов
                //Выполняем преобразования с текущей строкой матрицы A
                for (var j = 0; j < size; j++)
                    MatrixA[i, j] /= r;
                //Выполняем преобразование текущего элемента матрицы B
                MatrixB[i] /= r;
                //Вычитаем текущую строку из нижних
                for (var j = i + 1; j < size; j++)
                {
                    var p = MatrixA[j, index[i]];
                    for (var k = i; k < size; k++)
                        MatrixA[j, index[k]] -= MatrixA[i, index[k]] * p;
                    MatrixB[j] -= MatrixB[i] * p;
                    MatrixA[j, index[i]] = 0.0;
                }
            }
        }

        static void Backward()  //Обратный ход метода для вычисления неизвестных переменных
        {                       //Движение происходит по кадой строке снизу вверх
            for (var i = (int)size - 1; i >= 0; i--)
            {
                var x = MatrixB[i]; //Начальное значение переменной X
                for (var j = i + 1; j < size; j++) //Корректируем это значение
                    x -= MatrixX[index[j]] * MatrixA[i, index[j]];
                MatrixX[index[i]] = x;
            }
        }
    }
}
