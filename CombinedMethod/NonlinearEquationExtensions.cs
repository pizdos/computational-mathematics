﻿using System;
using HordeMethod;
using Models;
using NewtonMethod;
using NonlinearEquation;

namespace CombinedMethod
{
    public static class NonlinearEquationExtensions
    {
        public static Solution Solve(this NonlinearEquation.NonlinearEquation nonlinearEquation, int epsPow, double min,
            double max)
        {
            var accuracy = 1d / (10 * epsPow);

            var (root1, iterations1) = nonlinearEquation.Find(min, max, accuracy);
            return new Solution(root1, iterations1, 0, 0, 0, 0);
        }

        private static (double, int) Find(this NonlinearEquation.NonlinearEquation nonlinearEquation, double min,
            double max, double accuracy)
        {
            var left = nonlinearEquation.Left;
            var firstDerivedFactors = nonlinearEquation.Factors.GetDerivedFactors4();
            var secondDerivedFactors = firstDerivedFactors.GetDerivedFactors4();
            var secondDerive = secondDerivedFactors.FuncWithFactors();
            var iteration = 1;
            var err = Math.Abs(left(max) - left(min));

            var a = min;
            var b = max;

            if (secondDerive(b) * left(b) > 0 ||
                secondDerive(a) * left(a) < 0)
                for (; err > 2 * accuracy; iteration++)
                {
                    a = nonlinearEquation.HordeProcessElement(b, a);
                    b = nonlinearEquation.NewtonProcessElement(b);
                    err = Math.Abs(a - b);
                }
            else if (secondDerive(b) * left(b) < 0 ||
                     secondDerive(a) * left(a) > 0)
                for (; err > 2 * accuracy; iteration++)
                {
                    a = nonlinearEquation.NewtonProcessElement(a);
                    b = nonlinearEquation.HordeProcessElement(a, b);
                    err = Math.Abs(a - b);
                }
            else
                throw new NotImplementedException("Not richable state");

            return ((a + b) / 2, iteration);
        }
    }
}