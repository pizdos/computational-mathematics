﻿using System;

namespace NonlinearEquation
{
    public static class Factors4Extensions
    {
        
        public static Func<double, double> FuncWithFactors(this Factors4 coefficients)
        {
            return x =>
                coefficients.K1 * Math.Pow(x, 3) +
                coefficients.K2 * Math.Pow(x, 2) +
                coefficients.K3 * x +
                coefficients.K4;
        }

        public static Factors4 GetDerivedFactors4(this Factors4 coefficients)
        {
            return new Factors4(
                0,
                3 * coefficients.K1,
                2 * coefficients.K2,
                coefficients.K3
            );
        }

        public static (double, double) LocalizeRoots(this Factors4 factors)
        {
            var derivedFactors = GetDerivedFactors4(factors);
            return Helpers.SolveQuadratic(derivedFactors.K2, derivedFactors.K3, derivedFactors.K4);
        }
    }
}