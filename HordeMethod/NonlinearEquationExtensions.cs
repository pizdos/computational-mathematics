﻿using System;
using Models;
using NonlinearEquation;

namespace HordeMethod
{
    //-2.4142244292045554 - 1039
    //0.41423082379175596 - 16
    //2.0000450446592715 - 48
    public static class NonlinearEquationExtensions
    {

        public static Solution Solve(this NonlinearEquation.NonlinearEquation nonlinearEquation, int epsPow)
        {
            var (min, max) = nonlinearEquation.Factors.LocalizeRoots();
            var accuracy = 1d / (10 * epsPow);

            if (Math.Abs(max - min) < 0.00000000001)
            {
                var (root, iterations) = nonlinearEquation.FindRoot(Helpers.NegInfinity, Helpers.PosInfinity, accuracy);
                return new Solution(root, iterations, 0, 0, 0, 0);
            } // check mb same roots

            var (root1, iterations1) = nonlinearEquation.FindRoot(-3, min, accuracy);
            var (root2, iterations2) = nonlinearEquation.FindRoot(min, max, accuracy);
            var (root3, iterations3) = nonlinearEquation.FindRoot(max, 3, accuracy);
            return new Solution(root1, iterations1, root2, iterations2, root3, iterations3);
        }

        public static double HordeProcessElement(this NonlinearEquation.NonlinearEquation nonlinearEquation,
            double a, double b)
        {
            var left = nonlinearEquation.Left;
            return b - left(b)*(a-b)/(left(a)-left(b));
        }

        private static (double, int) FindRoot(this NonlinearEquation.NonlinearEquation nonlinearEquation, double min, double max, double accuracy)
        {
            var left = nonlinearEquation.Left; 
            var iteration = 1;
            var c = 0.0;
            var err = Math.Abs(left(max) - left(min));
            for (; err > accuracy && left(c) != 0; iteration++)
            {
                var prevC = c;
                c = nonlinearEquation.HordeProcessElement(min, max);
                if (left(min) * left(c) > 0)
                    min = c;
                else
                    max = c;
                err = Math.Abs(c - prevC);

                Console.WriteLine($"{iteration}//({min};{max}) ___ {c} ____ {err}\n");
            }

            return (c, iteration);
        }
    }
}