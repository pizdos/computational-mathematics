﻿using System;
using LinearAlgebraicEquations;
using SqrtMethod;

namespace SqrtMethod.App
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var linearAlgebraicEquationsSystem =
                new LinearAlgebraicEquationsSystem(new double[,]
                {
                    {1, 2, 3},
                    {2, 8, 4},
                    {3, 4, 11}
                }, new double[]
                {
                    -3, 2, 1
                });
            var solution = linearAlgebraicEquationsSystem.Solve();
        }
    }
}