﻿using System;
using GaussColumnMethod;
using LinearAlgebraicEquations;
using Matrix;

namespace SqrtMethod
{
    public static class LinearAlgebraicEquationsSystemExtensions
    {
        public static double[] Solve(this LinearAlgebraicEquationsSystem linearAlgebraicEquationsSystem)
        {
            if (!linearAlgebraicEquationsSystem.IsSymmetric())
                throw new ArgumentException("Need symmetric matrix");

            var uMatrix = linearAlgebraicEquationsSystem.A.CalculateUMatrix();
            var transposedUMatrix = uMatrix.Matrix2D.Transpose();
            var equationsSystemWithC = new LinearAlgebraicEquationsSystem(transposedUMatrix,
                linearAlgebraicEquationsSystem.B.Clone() as double[] ?? throw new InvalidOperationException());
            var cResults = equationsSystemWithC.AntiReverse();
            var finalEquationsSystem = new LinearAlgebraicEquationsSystem(uMatrix.Matrix2D, cResults);
            var results = finalEquationsSystem.Reverse();

            return results;
        }

        private static UMatrix CalculateUMatrix(this double[,] arr)
        {
            var size = arr.GetSize();
            var uMatrix = new UMatrix(size);
            for (var i = 0; i < size; i++)
            {
                for (var j = 0; j < size; j++)
                {
                    if (i == j)
                    {
                        double sum = 0;
                        for (var k = 0; k < i && i > 0; k++)
                        {
                            sum += Math.Pow(uMatrix[k, i], 2);
                        }

                        uMatrix[i, j] = Math.Sqrt(arr[i, j] - sum);
                    }
                    else if (j > i)
                    {
                        double sum = 0;
                        for (var k = 0; k < i && i > 0; k++)
                        {
                            sum += uMatrix[k, i] * uMatrix[k, j];
                        }

                        uMatrix[i, j] = (arr[i, j] - sum) / uMatrix[i, i];
                    }
                }
            }

            return uMatrix;
        }

        private static bool IsSymmetric(this LinearAlgebraicEquationsSystem linearAlgebraicEquationsSystem)
        {
            for (var i = 0; i < linearAlgebraicEquationsSystem.Size; i++)
            {
                for (var j = 0; j < linearAlgebraicEquationsSystem.Size; j++)
                {
                    if (Math.Abs(linearAlgebraicEquationsSystem.A[i, j] - linearAlgebraicEquationsSystem.A[j, i]) >
                        0.00000000001)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}