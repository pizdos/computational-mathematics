﻿namespace Matrix
{
    //  1  |  0  | ... |  0  |
    // l21 |  1  | ... |  0  |
    // ... | ... | ... | ... |
    // ln1 | ln2 | ... |  1  |
    public class LMatrix : Matrix
    {

        public LMatrix(int size) : base(size)
        {
        }

        public override double this[int i, int j]
        {
            get
            {
                if (j > i) return 0;
                if (i == j) return 1;
                return MatrixData[i][j];
            }
            set
            {
                if (i > j)
                    MatrixData[i][j] = value;
            }
        }
    }
}