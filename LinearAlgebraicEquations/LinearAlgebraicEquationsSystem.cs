﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using Matrix;

namespace LinearAlgebraicEquations
{
    public class LinearAlgebraicEquationsSystem
    {
        public readonly int Size;
        [DisallowNull] public readonly double[,] A;
        [DisallowNull] public readonly double[] B;

        public LinearAlgebraicEquationsSystem([NotNull] double[,] a, [NotNull] double[] b)
        {
            Size = b.Length;
            
            if (a.Rank != 2)
                throw new ArgumentException(nameof(a));
            if (a.LongLength / Size != Size)
                throw new ArgumentException(nameof(a));

            A = a;
            B = b;
        }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            for (var i = 0; i < Size; i++)
            {
                for (var j = 0; j < Size; j++)
                {
                    stringBuilder.Append($"{A[i, j]} ");
                }

                stringBuilder.Append(B[i]);
                if (i < Size-1)
                {
                    stringBuilder.Append(Environment.NewLine);   
                }
            }

            return stringBuilder.ToString();
        }

        public void SortRows(int column, int topRow)
        {
            var maxLine = topRow;
            for (var i = 1; i < Size; i++)
                if (Math.Abs(A[i, column]) > Math.Abs(A[maxLine, column]))
                    maxLine = i;
            if (maxLine != topRow)
                SwapLines(maxLine, topRow);
        }

        public void SwapLines(int line, int anotherLine)
        {
            A.SwapRows(line, anotherLine);
            B.SwapElements(line, anotherLine);
        }
    }
}