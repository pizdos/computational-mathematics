﻿using System;
using LinearAlgebraicEquations;
using Matrix;

namespace KhaletskyScheme.App
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var linearAlgebraicEquationsSystem =
                new LinearAlgebraicEquationsSystem(new double[,]
                {
                    {4, -3, 2},
                    {8, -8, 7},
                    {12, -5, 5}
                }, new double[]
                {
                    0, -12, 4
                });
            Console.WriteLine(linearAlgebraicEquationsSystem.A.IsDiagonallyDominant());
            if (linearAlgebraicEquationsSystem.A.IsDiagonallyDominant())
            {
                var solution = linearAlgebraicEquationsSystem.Solve();
            }
        }
    }
}