﻿using System;
using LinearAlgebraicEquations;

namespace SimpleIterationsMethod
{
    public static class LinearAlgebraicEquationsSystemExtensions
    {
        public static (double[], int) Solve(this LinearAlgebraicEquationsSystem linearAlgebraicEquationsSystem)
        {
            var size = linearAlgebraicEquationsSystem.Size;
            var results = new double[size];
            Array.Clear(results, 0, size-1);
            
            var iteration = 1;
            var accuracy = double.MaxValue;
            
            for (; err < accuracy; iteration++)
            {
                var newResults = linearAlgebraicEquationsSystem.CountNewResults(results);
                accuracy = Accuracy(newResults, results, size);
                prevX = x;
            }

            return (x, iteration);
            linearAlgebraicEquationsSystem.
        }

        private static double[] CountNewResults(this LinearAlgebraicEquationsSystem linearAlgebraicEquationsSystem,
            double[] results)
        {
            var newResults = new double[linearAlgebraicEquationsSystem.Size];
            for (var i = 0; i < linearAlgebraicEquationsSystem.Size; i++)
            {
                for (var j = 0; j < linearAlgebraicEquationsSystem.Size; j++)
                {
                    newResults[i] += linearAlgebraicEquationsSystem.A[i, j];
                }

                newResults[i] -= linearAlgebraicEquationsSystem.B[i];
            }

            return newResults;
        }
        
        private static double Accuracy(double[] vector, double[] previous, int size)
        {
            var accuracy = new double[size];
            double max = 0;

            for (var i = 0; i < size; i++)
            {
                accuracy[i] = vector[i] - previous[i];
                accuracy[i] = Math.Pow(accuracy[i], 2);
                if (accuracy[i] > max) max = accuracy[i];
            }

            max = Math.Sqrt(max);

            return max;
        }
    }
}