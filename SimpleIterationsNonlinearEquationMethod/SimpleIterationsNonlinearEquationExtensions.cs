﻿using System;
using Models;

namespace SimpleIterationsNonlinearEquationMethod
{
    public static class SimpleIterationsNonlinearEquationExtensions
    {
        public static Solution Solve(this SimpleIterationsNonlinearEquation simpleIterationsNonlinearEquation, int epsPow, double min, double max)
        {
            var accuracy = 1d / (10 * epsPow);

            var (root1, iterations1) = simpleIterationsNonlinearEquation.Find(min, max, accuracy);
            return new Solution(root1, iterations1, 0,0,0,0);
        }

        private static (double, int) Find(this SimpleIterationsNonlinearEquation simpleIterationsNonlinearEquation, double min,
            double max, double accuracy)
        {
            var iteration = 1;
            var fi = simpleIterationsNonlinearEquation.Fi;
            var err = Math.Abs(fi(max) - fi(min));

            var x = min;
            var prevX = max;
            
            for (; err > accuracy; iteration++)
            {
                x = fi(prevX);
                err = Math.Abs(x - prevX);
                prevX = x;
            }

            return (x, iteration);
        }
    }
}