﻿using System;
using LinearAlgebraicEquations;

namespace ThreeDiagonalMethod
{
    public static class ThreeDiagonalLinearAlgebraicEquationsSystemExtensions
    {
        public static double[] Solve(
            this ThreeDiagonalLinearAlgebraicEquationsSystem threeDiagonalLinearAlgebraicEquationsSystem)
        {
            var (delta, lambda) = threeDiagonalLinearAlgebraicEquationsSystem.Right();
            var results = threeDiagonalLinearAlgebraicEquationsSystem.Back(delta, lambda);
            return results;
        }

        private static (double[], double[]) Right(
            this ThreeDiagonalLinearAlgebraicEquationsSystem threeDiagonalLinearAlgebraicEquationsSystem)
        {
            var delta = threeDiagonalLinearAlgebraicEquationsSystem.Delta();
            var lambda = threeDiagonalLinearAlgebraicEquationsSystem.Lambda(delta);
            return (delta, lambda);
        }

        private static double[] Back(
            this ThreeDiagonalLinearAlgebraicEquationsSystem threeDiagonalLinearAlgebraicEquationsSystem, double[] delta, double[] lambda)
        {
            var lastIndex = threeDiagonalLinearAlgebraicEquationsSystem.Size-1;
            var results = new double[threeDiagonalLinearAlgebraicEquationsSystem.Size];
            for (var i = lastIndex; i > -1; i--)
            {
                var mul = i == lastIndex ? 0 : delta[i] * results[i + 1];
                results[i] = mul + lambda[i];
            }

            return results;
        }

        private static double[] Delta(
            this ThreeDiagonalLinearAlgebraicEquationsSystem threeDiagonalLinearAlgebraicEquationsSystem)
        {
            var size = threeDiagonalLinearAlgebraicEquationsSystem.Size;
            var delta = new double[size];
            for (var i = 0; i < size; i++)
            {
                var mul = i == 0 ? 0 : threeDiagonalLinearAlgebraicEquationsSystem.B[i] * delta[i - 1];
                delta[i] = -threeDiagonalLinearAlgebraicEquationsSystem.D[i] /
                           (threeDiagonalLinearAlgebraicEquationsSystem.C[i] + mul);
            }

            return delta;
        }

        private static double[] Lambda(
            this ThreeDiagonalLinearAlgebraicEquationsSystem threeDiagonalLinearAlgebraicEquationsSystem, double[] delta)
        {
            if (threeDiagonalLinearAlgebraicEquationsSystem.Size != delta.Length)
                throw new ArgumentException(nameof(delta));

            var size = threeDiagonalLinearAlgebraicEquationsSystem.Size;
            var lambda = new double[size];
            for (var i = 0; i < size; i++)
            {
                var mulLambda = i == 0 ? 0 : threeDiagonalLinearAlgebraicEquationsSystem.B[i] * lambda[i - 1];
                var mulDelta = i == 0 ? 0 : threeDiagonalLinearAlgebraicEquationsSystem.B[i] * delta[i - 1];
                lambda[i] = (threeDiagonalLinearAlgebraicEquationsSystem.R[i] - mulLambda) /
                            (threeDiagonalLinearAlgebraicEquationsSystem.C[i] + mulDelta);
            }

            return lambda;
        }
    }
}