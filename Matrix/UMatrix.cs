﻿namespace Matrix
{
    // u11 | u12 | ... | u1n |
    //  0  | u22 | ... | u2n |
    // ... | ... | ... | ... |
    //  0  |  0  | ... | unn |
    public class UMatrix : Matrix
    {

        public UMatrix(int size) : base(size)
        {
        }

        public override double this[int i, int j]
        {
            get
            {
                if (i > j) return 0;
                return MatrixData[Size - 1 - i][Size - 1 - j];
            }
            set
            {
                if (!(i > j))
                    MatrixData[Size - 1 - i][Size - 1 - j] = value;
            }
        }
    }
}