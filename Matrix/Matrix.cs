﻿namespace Matrix
{
    public abstract class Matrix
    {
        protected readonly double[][] MatrixData;
        public readonly int Size;

        protected Matrix(int size)
        {
            Size = size;
            MatrixData = new double[size][];
            for (var i = 0; i < size; i++)
            {
                MatrixData[i] = new double[i + 1];
                MatrixData[i][i] = 1;
            }
        }

        public double[,] Matrix2D
        {
            get
            {
                var result = new double[Size,Size];
                for (var i = 0; i < Size; i++)
                {
                    for (var j = 0; j < Size; j++)
                    {
                        result[i, j] = this[i, j];
                    }
                }

                return result;
            }
        }

        public abstract double this[int i, int j]
        {
            get;
            set;
        }
    }
}