﻿using System;

namespace Matrix
{
    public class SquareMatrix
    {
        public readonly double[,] MatrixData;
        public readonly int Size;

        public SquareMatrix(double[,] matrixData)
        {
            if (matrixData.GetLength(0) != matrixData.GetLength(1))
                throw new ArgumentException("not square matrix");
            
            MatrixData = matrixData;
            Size = matrixData.GetLength(0);
        }
    }
}